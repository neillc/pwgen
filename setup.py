import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pwgen-neillc",
    version="0.0.1",
    author="Neill Cox",
    author_email="neill@ingenious.com.au",
    description="A password generator",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/neillc/pwgen",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
)
