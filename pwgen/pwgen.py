import random
import string

import click

@click.command()
@click.option('--length', 'pw_length', default=12, help='password length')
@click.option('--ambiguous/--no-ambiguous', '-a/-A', default=False)
@click.option('--exclude', '-e', default=None)
@click.option('--punctuation/--no-punctuation', '-p/-P', default=True)
def generate(ambiguous, punctuation, exclude, pw_length):
    ambiguous_chars = '0O1l'
    delete_chars = string.whitespace

    if not ambiguous:
        delete_chars += ambiguous_chars

    if not punctuation:
        delete_chars += string.punctuation

    if exclude:
        delete_chars += exclude

    choices = string.printable.translate(''.maketrans(string.printable, string.printable, delete_chars))
    print(''.join(random.choices(choices, k=pw_length)))


if __name__ == '__main__':
    generate()